define [
	'goo/util/Grid'
], (
	Grid
) ->
	"use strict"

	return class Graveyard
		constructor: (goo, startingPosition, testing) ->
			testing = testing || false
			@goo = goo

			# root entity for graveyards
			@rootEntity = goo.world.createEntity('graveyard')
			@rootEntity.transformComponent.setTranslation(startingPosition.x, startingPosition.y, startingPosition.z)
			@rootEntity.addToWorld()

			@pieceSize = 4.5 # perhaps make this a parameter?
			@size = 7

			if testing
				grid = new Grid(goo.world,
					grids: [
						stepX: @size
						stepY: @size
						color: [1, 1, 1, 1]
						width: 1
					]
					floor: true
					width: 4 * @size
					height: 4 * @size
				)
				grid.topEntity.transformComponent.setTranslation(startingPosition.x - @size / 2, startingPosition.y, startingPosition.z - @size/2)
				grid.addToWorld()

			@positions = []
			for i in [-2...2]
				for j in [-2...2]
					@positions.push { x: i, z: j }

			@positions = @shuffle @positions
			@positions.sort (pos1, pos2) ->
				return (pos1.x + pos1.z) - (pos2.x + pos2.z)

			@fallen = {}

		buryPiece: (piece, entity) ->
			@fallen[piece.id] = piece

			next = @getNext()
			tc = entity.transformComponent
			@rootEntity.transformComponent.attachChild(tc)
			tc.transform.translation.x = next.x
			tc.transform.translation.z = next.z
			tc.setUpdated()

		getNext: ->
			nextPos = @positions.pop()
			x = nextPos.x * @size + (Math.random() - 0.5) * (@size - @pieceSize + 1)
			z = nextPos.z * @size + (Math.random() - 0.5) * (@size - @pieceSize + 1)

			return { x: x, z: z }

		# Fisher-Yates shuffle
		shuffle: (a) ->
			i = a.length
			while --i > 0
				j = ~~(Math.random() * (i + 1))
				t = a[j]
				a[j] = a[i]
				a[i] = t
			a
