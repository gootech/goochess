define ->
	"use strict"

	return class GameLogic
		constructor: ->
			# each square contains null or a piece
			@board = []
			for i in [0...8]
				@board.push []
				for j in [0...8]
					@board[i].push null

		process: (move) ->
			return { valid: true, move: move }

		addPiece: (piece, file, rank) ->
			@board[file][rank] = piece

		clearSquare: (file, rank) ->
			@addPiece(null, file, rank)

		movePiece: (fromFile, fromRank, toFile, toRank) ->
			piece = @board[fromFile][fromRank]
			unless piece
				console.error 'Cannot move piece that doesn\'t exist!'
				return null

			# get piece at destination, if there is one
			takenPiece = @board[toFile][toRank]
			if takenPiece
				@clearSquare(toFile, toRank)

			@clearSquare(fromFile, fromRank)
			@clearSquare(toFile, toRank)
			@addPiece(piece, toFile, toRank)

			piece.square.file = toFile
			piece.square.rank = toRank

			# return taken piece
			return takenPiece
