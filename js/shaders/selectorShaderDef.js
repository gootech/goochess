define([
	'goo/renderer/MeshData',
	'goo/renderer/Shader',
	'goo/renderer/shaders/ShaderFragment'
], function(
	MeshData,
	Shader,
	ShaderFragment
) {
	return {
		attributes : {
			vertexPosition : MeshData.POSITION,
			vertexUV0 : MeshData.TEXCOORD0
		},
		uniforms : {
			viewProjectionMatrix : Shader.VIEW_PROJECTION_MATRIX,
			worldMatrix : Shader.WORLD_MATRIX,
			diffuseMap : 'DIFFUSE_MAP',//Shader.TEXTURE0,
			color: [0.0, 0.0, 0.0]
		},
		vshader : [ //
		'attribute vec3 vertexPosition;', //
		'attribute vec2 vertexUV0;', //

		'uniform mat4 viewProjectionMatrix;',
		'uniform mat4 worldMatrix;',//

		'varying vec2 texCoord0;',//

		'void main(void) {', //
		'	texCoord0 = vertexUV0;',//
		'	gl_Position = viewProjectionMatrix * worldMatrix * vec4(vertexPosition, 1.0);', //
		'}'//
		].join('\n'),
		fshader : [//
		'precision mediump float;',//

		'uniform sampler2D diffuseMap;',//
		'uniform vec3 color;',//

		'varying vec2 texCoord0;',//

		'void main(void)',//
		'{',//
		' float alpha = texture2D(diffuseMap, texCoord0).a;',
		' if (alpha < 0.6) discard;',
		'	else gl_FragColor = vec4(color, alpha);',//
		'}'//
		].join('\n')
	};
});