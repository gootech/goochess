define [
	'goo/entities/EntityUtils',
	'goo/loaders/SceneLoader',
	'goo/loaders/MeshLoader',
	'goo/loaders/BundleLoader',
	'goo/loaders/DynamicLoader',
	'goo/renderer/Material',
	'goo/renderer/Texture',
	'goo/renderer/TextureCreator',
	'goo/shapes/ShapeCreator',
	'goo/renderer/shaders/ShaderLib',
	'goo/util/Latch',
	'goo/util/rsvp',
	'goo/util/Grid'
	'js/GameLogic',
	'js/ToledoChess',
	'js/Piece',
	'js/shaders/selectorShaderDef',
	'js/Graveyard'
], (
	EntityUtils,
	SceneLoader,
	MeshLoader,
	BundleLoader,
	DynamicLoader,
	Material,
	Texture,
	TextureCreator,
	ShapeCreator,
	ShaderLib,
	Latch,
	RSVP,
	Grid,
	GameLogic,
	ToledoChess,
	Piece,
	selectorShaderDef,
	Graveyard
) ->
	"use strict"

	return class GooChess
		constructor: (goo, squareSize) ->
			@goo = goo
			@squareSize = squareSize
			@ai = ToledoChess
			@ai.aiCallback = @aiCallback
			@ai.checkmateCallback = @checkmateCallback

			@blackMaterial = Material.createMaterial(ShaderLib.texturedLit, 'Black')
			colorInfo = new Uint8Array([32, 32, 32, 255])
			texture = new Texture(colorInfo, null, 1, 1)
			@blackMaterial.setTexture 'DIFFUSE_MAP', texture

			@whiteMaterial = Material.createMaterial(ShaderLib.texturedLit, 'White')
			colorInfo = new Uint8Array([255, 255, 255, 255])
			texture = new Texture(colorInfo, null, 1, 1)
			@whiteMaterial.setTexture 'DIFFUSE_MAP', texture

			# encompassing entity for the chess, to move easily
			@chessEntity = goo.world.createEntity('chessEntity')
			@chessEntity.addToWorld()

			quad = ShapeCreator.createQuad(1, 1)
			@selector = EntityUtils.createTypicalEntity(goo.world, quad)
			selMat = Material.createMaterial(selectorShaderDef, 'SelectMaterial')
			selMat.blendState.blending = 'CustomBlending'
			selMat.uniforms.color = [0, 0.5176470588235295, 0.8196078431372549]
			selMat.offsetState.enabled = true
			selMat.setTexture('DIFFUSE_MAP', new TextureCreator().loadTexture2D('resources/selected.png'))
			@selector.meshRendererComponent.materials.push selMat
			@selector.meshRendererComponent.isPickable = false
			@selector.transformComponent.transform.rotation.rotateX(-Math.PI / 2);

			@chessEntity.transformComponent.attachChild(@selector.transformComponent)

			@gameLogic = new GameLogic()
			# maps piece ID's to corresponding entities
			@pieceMap = {}

			@blackGraveyard = new Graveyard(goo, { x: -29, y: -2, z: -11 })
			@whiteGraveyard = new Graveyard(goo, { x: 29, y: -2, z: 11 })

			# Hold piece meshes to create pieces
			@meshes = {}
			pieces = ['pawn', 'king', 'queen', 'bishop', 'knight', 'rook']

			totalObjects = 580

			showProgress()

			# Create board and pieces after bundles have been loaded
			latch = new Latch(
				totalObjects
				{
					done: =>
						scale = 0.44

						transform = @chessEntity.transformComponent.transform
						transform.translation.setd(- scale * @squareSize/2 + 0.5, 0, scale * @squareSize/2)
						transform.scale.set(scale, scale, scale)
						transform.setRotationXYZ(0, Math.PI/2, 0)

						transform = @whiteGraveyard.rootEntity.transformComponent.transform
						transform.scale.set(scale, scale, scale)

						transform = @blackGraveyard.rootEntity.transformComponent.transform
						transform.scale.set(scale, scale, scale)

						@createBoard()
						@createPieces()
						@selector.transformComponent.transform.scale.set(7, 7, 7)
						@selector.addToWorld()

						hideOverlay()

						@goo.doRender = true
						@goo.startGameLoop()
				}
			)

			# Load resources
			resourcePath = 'resources/'
			loader = new BundleLoader({ rootPath: resourcePath })

			loader.loadBundle('pieces.bundle').then =>
				promises = []
				meshLoader = new MeshLoader({ loader: loader })

				for piece in pieces
					console.log 'Loading', piece
					do (piece) =>
						promises.push meshLoader.load( "meshes/" + piece + ".mesh").then(
							(mesh) =>
								@meshes[piece] = mesh
							, (err) -> console.log 'Error!', err
						)

				RSVP.all(promises).then(
					=>
						latch.countDown()
					, (err) -> console.error "Error!", err
				)

			dynamicLoader = new DynamicLoader({ rootPath: resourcePath, world: goo.world })

			dynamicLoader.load(
				'test.scene',
				{
					progressCallback: (loaded, totalObjects) ->
						window.updateProgress(loaded / totalObjects)
					beforeAdd: (entity) ->
						latch.countDown()
						return true
				}
			).then (configs) ->
				console.log 'Loaded scene.'
			, (err) -> console.error "Error!", err

		# callback used by the Toledo AI to move the pieces
		aiCallback: (player, from, to) =>
			#console.log player, from, to

			fromCoord = {}
			toCoord = {}
			
			# for conversion rules see ToledoCalc.txt
			fromCoord.file = 9-Math.floor(from/10)
			fromCoord.rank = (from%10)-1
			toCoord.file = 9-Math.floor(to/10)
			toCoord.rank = (to%10)-1
			@hideSelector()
			@move(fromCoord, toCoord)

		# callback used when a checkmate occurs
		checkmateCallback: (who) ->
			victoryBox = $('#victory-box')
			if who == 1
				victoryBox.text('You won!')
			else
				victoryBox.text('You lost!')

			victoryBox.addClass(if who == 1 then 'white' else 'black').show()

		select: (square) ->
			# for conversion rules see ToledoCalc.txt
			try
				piece = @gameLogic.board[square.file][square.rank]
				@selectPiece(square) if piece?

				@ai.OnClick((9-square.file)*10 + square.rank+1)
			catch error
				console.error "Error! #{error}"

		selectPiece: (square) ->
			squareCoordinates = @squareCoordinates(square.file, square.rank)
			@selector.transformComponent.setTranslation(squareCoordinates.x, 0.3, squareCoordinates.z)

		hideSelector: ->
			@selector.getComponent('transformComponent').setTranslation(0, 0, 0)

		move: (fromCoord, toCoord) ->
			piece = @gameLogic.board[fromCoord.file][fromCoord.rank]
			return unless piece?

			entity = @pieceMap[piece.id]
			# TODO error unless entity

			newTranslation = @squareCoordinates(toCoord.file, toCoord.rank)
			entity.getComponent('transformComponent').setTranslation(newTranslation.x, newTranslation.y, newTranslation.z)

			takenPiece = @gameLogic.movePiece(fromCoord.file, fromCoord.rank, toCoord.file, toCoord.rank)
			if takenPiece
				(if takenPiece.color == Piece.BLACK then @blackGraveyard else @whiteGraveyard).buryPiece(takenPiece, @pieceMap[takenPiece.id])
				delete @pieceMap[takenPiece.id]

		createBoard: ->
			black = true
			for file in [0...8]
				for rank in [0...8]
					black = !black unless rank is 0
					@createSquare(black, file, rank)

		squareCoordinates: (file, rank) ->
			{ x: (4 - file)*@squareSize, y: 0, z: (4 - rank)*@squareSize }

		createSquare: (black, file, rank) ->
			meshData = ShapeCreator.createBox(@squareSize, 0.2, @squareSize)

			entity = EntityUtils.createTypicalEntity(@goo.world, meshData)
			entity.name = "square (#{file}, #{rank})"

			coordinates = @squareCoordinates(file, rank)
			@chessEntity.transformComponent.attachChild(entity.transformComponent)
			entity.transformComponent.setTranslation(coordinates.x, coordinates.y, coordinates.z)
			entity.meshRendererComponent.materials.push if black then @blackMaterial else @whiteMaterial
			entity.meshRendererComponent.isPickable = true

			entity.coordinates = {file: file, rank: rank}

			entity.addToWorld()

		createPieces: ->
			black = true
			# create black pieces
			for i in [0...8]
				@createPiece(black, 6,i, 'pawn')

			@createPiece(black, 7,2, 'bishop')
			@createPiece(black, 7,5, 'bishop')

			@createPiece(black, 7,3, 'queen')
			@createPiece(black, 7,4, 'king')

			@createPiece(black, 7,1, 'knight')
			@createPiece(black, 7,6, 'knight')

			@createPiece(black, 7,0, 'rook')
			@createPiece(black, 7,7, 'rook')

			# create white pieces
			black = false
			for i in [0...8]
				@createPiece(black, 1,i, 'pawn')

			@createPiece(black, 0,2, 'bishop')
			@createPiece(black, 0,5, 'bishop')

			@createPiece(black, 0,3, 'queen')
			@createPiece(black, 0,4, 'king')

			@createPiece(black, 0,1, 'knight')
			@createPiece(black, 0,6, 'knight')

			@createPiece(black, 0,0, 'rook')
			@createPiece(black, 0,7, 'rook')


		createPiece: (black, file, rank, type) ->
			if @meshes.hasOwnProperty(type)
				meshData = @meshes[type]
				coordinates = @squareCoordinates(file, rank)
				entity = EntityUtils.createTypicalEntity(@goo.world, meshData)
				entity.name = type
				@chessEntity.transformComponent.attachChild(entity.transformComponent)
				entity.transformComponent.setTranslation(coordinates.x, coordinates.y, coordinates.z)
				entity.transformComponent.transform.scale.set(2,2,2)
				entity.meshRendererComponent.materials.push if black then @blackMaterial else @whiteMaterial
				entity.meshRendererComponent.isPickable = true

				colour = if black then Piece.BLACK else Piece.WHITE
				piece = new Piece(type, colour, { file: file, rank: rank })
				@gameLogic.addPiece(piece, file, rank)

				@pieceMap[piece.id] = entity

				entity.addToWorld()

		pieceFromEntity: (entity) ->
			# forgive me Lord
			for row in @gameLogic.board
				for piece in row
					if piece? and @pieceMap[piece.id] == entity
						return piece

			return null
