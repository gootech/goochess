require.config
	baseUrl: './'
	#waitSeconds: 15
	paths:
		'goo': '/js/goo',
		'goo/lib': '/js/goo/lib'

require [
	'goo/entities/GooRunner',
	'goo/entities/components/CameraComponent',
	'goo/entities/components/LightComponent',
	'goo/entities/systems/PickingSystem',
	'goo/entities/EntityUtils',
	'goo/renderer/light/PointLight',
	'goo/renderer/Camera',
	'goo/renderer/Material',
	'goo/renderer/shaders/ShaderLib',
	'goo/entities/components/ScriptComponent',
	'goo/scripts/OrbitCamControlScript'
	'goo/shapes/ShapeCreator',
	'goo/math/Vector3',
	'goo/math/Ray',
	'goo/picking/PrimitivePickLogic',
	'js/GooChess'
], (
	GooRunner,
	CameraComponent,
	LightComponent,
	PickingSystem,
	EntityUtils,
	PointLight,
	Camera,
	Material,
	ShaderLib,
	ScriptComponent,
	OrbitCamControlScript,
	ShapeCreator,
	Vector3,
	Ray,
	PrimitivePickLogic,
	GooChess
) ->
	"use strict"

	picked = null

	window.showProgress = ->
		$('#loadingOverlay .progressBar').show()
		updateProgress(0)

	window.updateProgress = (progress)->
		progressPercentage = (100 * progress).toFixed()+'%'
		$('#loadingOverlay .progressBar .progress').css('width', progressPercentage)
		$('#loadingOverlay .progressBar .progressText').text(progressPercentage)
		#console.debug $('#loadingOverlay .progressBar .progress').css('width')

	window.hideOverlay = ->
		$('#loadingOverlay').hide()

	window.hideProgress = ->
		$('#loadingOverlay .progressBar').hide()

	init = ->
		goo = new GooRunner({
			antialias: true
			manuallyStartGameLoop: true
		});
		goo.renderer.domElement.id = 'goo'
		goo.doRender = false
		document.body.appendChild(goo.renderer.domElement)
		goo.renderer.setClearColor(128/255, 128/255, 128/255, 1.0)

		gooChess = new GooChess(goo, 10)

		# Setup camera
		camera = new Camera(45, 1, 1, 3000)
		cameraEntity = goo.world.createEntity("CameraEntity")
		cameraComponent = new CameraComponent(camera)
		cameraComponent.isMain = true
		cameraEntity.setComponent(cameraComponent)

		cameraEntity.setComponent(new ScriptComponent(new OrbitCamControlScript({
			domElement : goo.renderer.domElement,
			spherical: new Vector3(60, -Math.PI/2, Math.PI/4),
			minAscent: 0,
			maxZoomDistance: 200,
			minZoomDistance: 10,
			dragButton: 2
		})))

		cameraEntity.addToWorld()

		picking = new PickingSystem()
		picking.setPickLogic(new PrimitivePickLogic())
		goo.world.setSystem picking

		picking.onPick = (pickedList) ->
			if pickedList and pickedList.length
				picked = pickedList[0].entity
			else
				picked = null

		goo.callbacks.push((tpf) ->
			if picked?
				if picked.coordinates?
					# selected square
					gooChess.select(picked.coordinates)
				else
					piece = gooChess.pieceFromEntity(picked)
					if piece?
						# selected piece
						gooChess.select(piece.square)

				picked = null
		)

		# Setup light
		entity = goo.world.createEntity('light')
		light = new PointLight()
		entity.transformComponent.transform.translation.set(0, 50, 0)
		entity.setComponent(new LightComponent(light))
		entity.addToWorld()

		hammer = new Hammer(document).on(
			'tap'
			(event) ->
				event.preventDefault()
				event.stopPropagation()

				mouseDownX = event.gesture.center.pageX;
				mouseDownY = event.gesture.center.pageY;
				ray = new Ray()
				camera.getPickRay(mouseDownX, mouseDownY, goo.renderer.viewportWidth, goo.renderer.viewportHeight, ray)
				picking.pickRay = ray
				picking._process()
			false
		)

	init()
