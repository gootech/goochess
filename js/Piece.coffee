define ->
	"use strict"

	return class Piece
		@lastId = 0
		constructor: (type, color, square) ->
			@id = Piece.lastId++
			@type = type
			@color = color
			@square = square # necessary?

		@BLACK = 0
		@WHITE = 1
