#!/bin/bash
loc="packed_goochess"
gooJS="../GooJS/build"

# Make basedir
mkdir $loc

# Copy html and resource files
cp index.html $loc/
cp -R resources $loc/
cp -R style $loc/

# Copy non minified javascript
mkdir $loc/js
cp -R js/* $loc/js

# Compile CoffeeScript
coffee -c $loc/js/

# Minify javascript
node $gooJS/build --path $loc/ --name js/main.js --out goochess_opt.js --closure

# Cleanup build directory
rm -frv $loc/js/*.js $loc/js/*.coffee $loc/js/shaders

# Move optimized code into packaged dir
mv goochess_opt.js $loc/js/

# Change javascript tags in html
#perl -pi -e 's/data-main="js\/main" src="js\/lib\/require.js"/src="js\/goochess_opt.js"/g' $loc/index.html
