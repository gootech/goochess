attribute vec3 vertexPosition;
#ifdef NORMAL
attribute vec3 vertexNormal;
#endif
#ifdef TANGENT
attribute vec4 vertexTangent;
#endif
#ifdef COLOR
attribute vec4 vertexColor;
#endif
#ifdef TEXCOORD0
attribute vec2 vertexUV0;
#endif
#ifdef TEXCOORD1
attribute vec2 vertexUV1;
#endif
uniform mat4 viewProjectionMatrix;
uniform mat4 worldMatrix;
uniform vec3 cameraPosition;
#ifndef MAX_POINT_LIGHTS
#define MAX_POINT_LIGHTS 0
#endif
#ifndef MAX_SPOT_LIGHTS
#define MAX_SPOT_LIGHTS 0
#endif
#if MAX_POINT_LIGHTS > 0
uniform vec4 pointLight[MAX_POINT_LIGHTS];
varying vec4 vPointLight[MAX_POINT_LIGHTS];
#endif
#if MAX_SPOT_LIGHTS > 0
uniform vec4 spotLight[MAX_SPOT_LIGHTS];
varying vec4 vSpotLight[MAX_SPOT_LIGHTS];
#endif
varying vec3 vWorldPos;
varying vec3 viewPosition;
#ifdef NORMAL
varying vec3 normal;
#endif
#ifdef TANGENT
varying vec3 binormal;
varying vec3 tangent;
#endif
#ifdef COLOR
varying vec4 color;
#endif
#ifdef TEXCOORD0
varying vec2 texCoord0;
#endif
#ifdef TEXCOORD1
varying vec2 texCoord1;
#endif

void main(void) {
	vec4 worldPos = worldMatrix * vec4(vertexPosition, 1.0);
	vWorldPos = worldPos.xyz;
	gl_Position = viewProjectionMatrix * worldPos;

	#ifdef NORMAL
		normal = normalize((worldMatrix * vec4(vertexNormal, 0.0)).xyz);
	#endif
	#ifdef TANGENT
		tangent = normalize((worldMatrix * vec4(vertexTangent.xyz, 0.0)).xyz);
		binormal = cross(normal, tangent) * vec3(vertexTangent.w);
	#endif
	#ifdef COLOR
		color = vertexColor;
	#endif
	#ifdef TEXCOORD0
		texCoord0 = vertexUV0;
	#endif
	#ifdef TEXCOORD1
		texCoord1 = vertexUV1;
	#endif

	#if MAX_POINT_LIGHTS > 0
		for(int i = 0; i < MAX_POINT_LIGHTS; i++) {
			vec3 lightVec = pointLight[i].xyz - worldPos.xyz;
			float lightDist = 1.0 - min((length(lightVec) / pointLight[i].w), 1.0);
			vPointLight[i] = vec4(lightVec, lightDist);
		}
	#endif
	#if MAX_SPOT_LIGHTS > 0
		for(int i = 0; i < MAX_SPOT_LIGHTS; i++) {
			vec3 lightVec = spotLight[i].xyz - worldPos.xyz;
			float lightDist = 1.0 - min((length(lightVec) / spotLight[i].w), 1.0);
			vSpotLight[i] = vec4(lightVec, lightDist);
		}
	#endif

	viewPosition = cameraPosition - worldPos.xyz;
}
