{
"attributes":{
"vertexColor":"COLOR",
"vertexNormal":"NORMAL",
"vertexPosition":"POSITION",
"vertexTangent":"TANGENT",
"vertexUV0":"TEXCOORD0",
"vertexUV1":"TEXCOORD1"
},
"fshaderRef":"shaders/uberShader.frag",
"name":"UberShader",
"processors":[
"uber",
"light"
],
"uniforms":{
"cameraPosition":"CAMERA",
"diffuseMap":"DIFFUSE_MAP",
"normalMap":"NORMAL_MAP",
"specularMap":"SPECULAR_MAP",
"viewProjectionMatrix":"VIEW_PROJECTION_MATRIX",
"worldMatrix":"WORLD_MATRIX"
},
"vshaderRef":"shaders/uberShader.vert"
}